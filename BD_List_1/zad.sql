-- 1) Pobierz wszystkie produkty oraz nazwy ich kategorii, przy czym nazwy produktów
-- powinny kończyć się literą „p”, a nazwy kategorii zaczynać na literę „c”.

SELECT products.*, categories.CategoryName
FROM products JOIN categories
WHERE products.ProductName like "%p" AND
categories.CategoryName like "c%";

-- 2) Pobierz nazwę i cenę jednostkową takich produktów, których cena jednostkowa wynosi
-- 20.00 lub 50.00 lub jest pusta.

SELECT ProductID, ProductName, CategoryID, UnitPrice
FROM products
ORDER BY CategoryID DESC, UnitPrice ASC;

-- 3) Pobierz identyfikator produktu, jego nazwę, kategorię do której należy oraz cenę
-- jednostkową z tabeli Products. Wynik ma być uporządkowany malejąco po kategorii, oraz
-- rosnąco wewnątrz każdej kategorii po cenie jednostkowej.

SELECT ProductName, UnitPrice
FROM products
WHERE UnitPrice = 20.00 OR UnitPrice = 50.00 OR UnitPrice = NULL;

-- 4) Wyświetl wszystkie regiony oraz liczbę dostawców, którzy pochodzą z tego regionu.

SELECT Region, COUNT(SupplierID)
FROM suppliers
GROUP BY Region;

-- 5) Pobierz identyfikatory sześciu produktów z tabeli Order Details, których kwota na
-- zamówieniu (ilość * cena jednostkowa) jest najmniejsza.

SELECT ProductID
FROM order_details
GROUP BY ProductID
ORDER BY (UnitPrice * Quantity) ASC
LIMIT 6;

-- 6) Wybierz listę takich identyfikatorów produktów z tabeli Order Details, których średnia
-- ilości zamówień jest większa niż 2.

select ProductID, count(OrderID)
from order_details
group by ProductID
having count(OrderID) > 2;

-- 7) Pobierz nazwiska pracowników i numery ich zamówień, które to zamówienia zostały
-- wystawione przed 23 stycznia 1998 roku.

select employees.LastName, orders.OrderID --, orders.OrderDate
from employees inner join orders on OrderID
where orders.OrderDate < '1998-01-23';

-- 8) Wypisz wszystkich klientów wraz z datami zamówień przez nich złożonych. Uwzględnij
-- także tych klientów, którzy nic nie zamówili.

select customers.CustomerID, orders.OrderDate
from customers left outer join orders on customers.CustomerID=orders.CustomerID;

-- 9) Wyświetl wszystkie możliwe kombinacje wierszy pomiędzy tabelami Shippers i Suppliers,
-- ale tylko takie, że nazwy firm w tych tabelach rozpoczynają się na literę „s”.

select * 
from shippers cross join suppliers;

-- 10) Pobierz nazwy produktu oraz datę umieszczenia zamówienia dla danego produktu.

select products.ProductName, orders.OrderDate
from (order_details
inner join orders
on order_details.OrderID = orders.OrderID)
inner join products 
on products.ProductID = order_details.ProductID;

-- 11) Wyświetl takie pary nazw terytoriów, którzy pochodzą z tego samego regionu. Pary nie
-- powinny się powtarzać.

select distinct Country, Region
from suppliers
where Region > ''
group by Region, Country;

-- 12) Wypisz listę identyfikatorów klientów, zamówione przez nich towary oraz sumę kwot
-- zamówień dla tych towarów.

select customers.CustomerID, order_details.ProductID,
 (order_details.UnitPrice * order_details.Quantity) * 1 - order_details.Discount as "Paid"
from customers inner join orders on customers.CustomerID = orders.CustomerID
inner join order_details on order_details.OrderID = orders.OrderID
group by customers.CustomerID, order_details.ProductID;

-- 13) Wypisz identyfikatory produktów i najmniejszą ilość występującą na zamówieniu dla tych
-- produktów.

select products.ProductID, min(order_details.Quantity)
from products inner join order_details on products.ProductID = order_details.ProductID
group by products.ProductID;

-- 14) Pobierz listę wszystkich firm, któe nie złożyły zamówienia 15 maja 1997. Nie wolno
-- użyć instrukcji JOIN.

select new_customers.CompanyName from
(
select customers.CompanyName, customers.CustomerID
from customers
union
select '', orders.CustomerID
from orders
where orders.OrderDate != date("1997-05-15")
) as new_customers
where new_customers.CompanyName > '';

-- 15) Na podstawie tabeli Customer utwórz tabelę tymczasową, a następnie wyświetl wszystkie
-- wiersze tej tabeli. W tabeli tymczasowej powinni się znaleźć tylko tacy klienci, których
-- nazwa rozpoczyna się na literę t.

create temporary table if not exists temp_clients as 
(
	select * from customers
	where ContactName like "t%"
);

-- 16) Usuń wiersze z tablicy Order Details dla takich zamówień, które powstały 14 kwietnia
-- 1998 oraz 17 lipca 1999.

create temporary table if not exists temp_order_details as
(
	select * from order_details
);

delete 
from temp_order_details
using temp_order_details inner join orders on temp_order_details.OrderID = orders.OrderID
where orders.OrderDate = "1998-04-14" or orders.OrderDate = "1999-07-17";

drop table if exists temp_order_details;

-- 17) Dodaj do tablicy Order Details wiersz z takim OrderId, który będzie odpowiadał OrderId
-- zamówieniu z tabeli Orders o dacie 14 kwietnia 1998. OrderId należy pobrać z tabeli Orders
-- za pomocą wyrażenia SELECT, a nie wpisywać jawnie.

create temporary table if not exists temp_order_details as
(
	select * from order_details
);

insert
into temp_order_details
select 0, orders.OrderID, 0, 0, 0, 0
from orders
where orders.OrderDate = "1998-04-14";

select * from temp_order_details
where ProductID = 0;

drop table if exists temp_order_details;


-- 18) Zwiększ cenę jednostkową o 2 dla wszystkich produktów dostarczanych przez dostawców
-- z USA.

create temporary table if not exists temp_products as
(
	select * from products
);

update temp_products inner join suppliers on temp_products.SupplierID = suppliers.SupplierID
set temp_products.UnitPrice = temp_products.UnitPrice + 2
where suppliers.Country = "USA";

drop table if exists temp_products;


-- 19) Do tabeli Products dodaj kolumnę TotalSales. Do dodanej kolumny, dla każdego
-- produktu wpisz sumę ilości na zamówieniach dla tego produktu.

-- 20) Usuń z tabeli Products dodaną w zadaniu poprzednim kolumnę.

create temporary table if not exists temp_products as
(
	select * from products
);

alter table temp_products
add TotalSales DOUBLE;

update temp_products
inner join 
(
select ProductID, sum((order_details.UnitPrice * order_details.Quantity) * 1 - order_details.Discount) as Total
from order_details
group by ProductID
) as product_orders
on product_orders.ProductID = temp_products.ProductID
set temp_products.TotalSales = product_orders.Total;

alter table temp_products
drop column TotalSales;

drop table if exists temp_products;
