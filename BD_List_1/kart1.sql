-- ZAD1
create temporary table if not exists temp_clients as 
(
	select * from customers
	where ContactName like "t%"
);

select * from temp_clients;

-- ZAD2
-- Pozwoliłem sobi edla bezpieczeństwa robić to w tabeli 
-- tymczasowej

create temporary table if not exists temp_products as
(
	select * from products
);

alter table temp_products
add TotalSales DOUBLE;

update temp_products
inner join 
(
select ProductID, 
sum((order_details.UnitPrice * order_details.Quantity) * 1 - order_details.Discount) as Total
from order_details
group by ProductID
) as product_orders
on product_orders.ProductID = temp_products.ProductID
set temp_products.TotalSales = product_orders.Total;

drop table if exists temp_products;

-- ZAD3
-- Tak samo, wolę na tymczasowych

create temporary table if not exists temp_products as
(
	select * from products
);

insert into temp_products (SupplierName, SupplierID)
select "Xaro Xhoan Ducksauce", suppliers.CompanyID
from suppliers
where suppliers.CompanyName = "Exotic Liquids";

select * from temp_products
where ProductName = "Xaro Xhoan Ducksauce";