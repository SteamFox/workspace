package net.steamfox.converter;

public class Converter
{
	private int _val;
	
	public void Set(int newVal)
	{
		_val = newVal;
	}
	
	public Converter(int newVal) 
	{
		Set(newVal);
	}
	
	public Converter()
	{
		this(0);
	}
	
	public String Convert(int base) throws Converter_BadBase
	{
		if(base < 2 || base > 16) throw new Converter_BadBase();
		StringBuilder RetR = new StringBuilder();
		
		for(int i = _val, mod = 0; i > 0; i = (i - mod) / base)
		{
			mod = i % base;
			RetR.insert(0, GetInputAsHex(mod));
		}
		
		if(base == 2) RetR.insert(0,  "0b");
		else if(base == 8) RetR.insert(0, '0');
		else if(base == 16) RetR.insert(0, "0x");
		
		return RetR.toString();
	}
	
	public static char GetInputAsHex(int in)
	{
		switch(in) 
		{
		case 10: return 'A';
		case 11: return 'B';
		case 12: return 'C';
		case 13: return 'D';
		case 14: return 'E';
		case 15: return 'F';
		default:
			if(in < 0 || in > 15) return '$';
			return (char)(in + '0');
		}
	}
}
