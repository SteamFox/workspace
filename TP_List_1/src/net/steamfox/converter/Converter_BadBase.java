package net.steamfox.converter;

public class Converter_BadBase extends Exception {
	
	private int _min, _max;
	
	public Converter_BadBase(int min, int max) 
	{
		_min = min;
		_max = max;
	}
	
	public Converter_BadBase() 
	{
		this(2, 16);
	}
	
	@Override
	public String getMessage() 
	{
		return new String
				("Passed bad base argument. Base must be within " + _min + " and " + _max);
	}
}
