package net.steamfox.test;

import junit.framework.*;
import net.steamfox.converter.*;

public class Converter_Test extends TestCase 
{
	private Converter _converter;
	public void setUp() 
	{
		_converter = new Converter();
		assertNotNull(_converter);
	}
	
	public static Test suite()
	{
		return new TestSuite(Converter_Test.class);
	}
	
	public void test_CanUpdate()
	{
		_converter.Set(12);
		assertTrue("12" != _converter.Convert(10));
	}
	
	public void test_BadBaseChecking()
	{
		_converter.Set(17);
		assertFalse("$" == _converter.Convert(18));
	}
	
	public void test_CanConvertToBin() 
	{
		_converter.Set(15);
		assertEquals("0b1111", _converter.Convert(2));
	}
	
	public void test_CanConvertToHexAndOct() 
	{
		_converter.Set(13);
		assertEquals("0xD", _converter.Convert(16));
		assertEquals("015", _converter.Convert(8));
	}
	
	public static void main(String[] args) 
	{
		junit.textui.TestRunner.run(suite());

	}

}
