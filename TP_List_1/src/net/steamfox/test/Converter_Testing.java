package net.steamfox.test;

import junit.framework.*;
import net.steamfox.converter.Converter;
import net.steamfox.converter.Converter_BadBase;

public class Converter_Testing extends TestCase 
{
	private Converter _converter;
	
	public void setUp()
	{
		_converter = new Converter();
	}
	
	public static Test suite()
	{
		return new TestSuite(Converter_Testing.class);
	}
	
	public static void main(String[] args)
	{
		junit.textui.TestRunner.run(suite());
	}
	
	public void test_IsInitialized()
	{
		assertNotNull(_converter);
	}
	
	public void test_ThrowsOnBadBase() 
	{
		try 
		{
			_converter.Convert(17);
		}
		catch(Converter_BadBase badBase)
		{
			return;
		}
		fail();
	}
	
	public void test_DoesAddPrefixes()
	{
		_converter.Set(10);
		try 
		{
			assertTrue(_converter.Convert(2).charAt(0) == '0' &&
					_converter.Convert(8).charAt(0) == '0' &&
					_converter.Convert(16).charAt(0) == '0');
		}
		catch (Converter_BadBase badBase) 
		{
			fail();
		}
	}
	
	public void  test_IsConvertingCorrectly()
	{
		_converter.Set(10);
		try 
		{
			assertEquals("0xA", _converter.Convert(16));
			assertEquals("012", _converter.Convert(8));
			assertEquals("10", _converter.Convert(10));
			assertEquals("0b1010", _converter.Convert(2));
		} 
		catch (Converter_BadBase e) 
		{
			fail();
		}
	}
	
	public void test_CreatesString() 
	{
		try 
		{
			String n = _converter.Convert(12);
			assertNotNull(n);
			n = null;
			assertNull(n);
		}
		catch (Converter_BadBase e) 
		{
			fail();
		}
	}
	
	public void test_ChangesContentsCorrectly()
	{
		_converter.Set(13);
		try 
		{
			assertFalse(_converter.Convert(10) == "0");
		} 
		catch (Converter_BadBase e) 
		{
			fail();
		}
	}
	
	public void test_()
	{
		assertNotNull(_converter);
	}
}
