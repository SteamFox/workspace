package net.steamfox.test4;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.steamfox.converter.Converter;

public class Converter_Test4 
{

	private Converter _converter;
	
	@Before
	public void setUp()
	{
		_converter = new Converter();
	}
	
	@Test
	public void test_IsSetUpCorrectly()
	{
		assertNotNull(_converter);
	}
	
	@Test
	public void test_CanUpdate()
	{
		_converter.Set(312);
		assertTrue("312" != _converter.Convert(10));
	}
	
	@Ignore
	@Test
	public void test_BadBaseChecking()
	{
		_converter.Set(17);
		assertFalse("$" == _converter.Convert(18));
	}
	
	@Test(timeout = 1)
	public void test_PointlessTest()
	{
		while(true)
		{
			_converter.Set(2);
		}
	}
	
	@Test
	public void test_CanConvertToBin() 
	{
		_converter.Set(15);
		assertEquals("0b1111", _converter.Convert(2));
	}
	
	@Test
	public void test_CanConvertToHexAndOct() 
	{
		_converter.Set(13);
		assertEquals("0xD", _converter.Convert(16));
		assertEquals("015", _converter.Convert(8));
	}
	
	@After
	public void tearDown()
	{
		_converter = null;
	}
}
