package net.steamfox.test4;

import static org.junit.Assert.*;
import org.junit.*;

import net.steamfox.converter.Converter;
import net.steamfox.converter.Converter_BadBase;

public class Converter_Testing4 
{
	private Converter _converter;

	@Before
	public void setUp()
	{
		_converter = new Converter();
	}
	
	@Test
	public void test_IsInitialized()
	{
		assertNotNull(_converter);
	}
	
	@Test(expected = Converter_BadBase.class)
	public void test_ThrowsOnBadBase() throws Converter_BadBase 
	{
		_converter.Convert(17);
	}
	
	@Test
	public void test_DoesAddPrefixes()
	{
		_converter.Set(10);
		try 
		{
			assertTrue(_converter.Convert(2).charAt(0) == '0' &&
					_converter.Convert(8).charAt(0) == '0' &&
					_converter.Convert(16).charAt(0) == '0');
		}
		catch (Converter_BadBase badBase) {
			fail();
		}
	}
	
	@Ignore
	@Test
	public void  test_IsConvertingCorrectly()
	{
		_converter.Set(10);
		try 
		{
			assertEquals("0xA", _converter.Convert(16));
			assertEquals("012", _converter.Convert(8));
			assertEquals("10", _converter.Convert(10));
			assertEquals("0b1010", _converter.Convert(2));
		} 
		catch (Converter_BadBase e) {
			fail();
		}
	}
	
	@Ignore
	@Test(timeout = 1)
	public void test_ChangesContentsCorrectly()
	{
		_converter.Set(13);
		try 
		{
			assertFalse(_converter.Convert(10) == "0");
		} 
		catch (Converter_BadBase e) {
			fail();
		}
		for(;;){}
	}
	
	@After
	public void tearDown()
	{
		_converter = null;
	}
}
