package net.steamfox.func;

public class Card
{
	// FIELDS
	
	private final char _symbol;
	private final char _color;
	
	private Player _player;
	
	// C's & D's
	
	public Card(char symbol, char color)
	{
		_color = color;
		_symbol = symbol;
	}
	
	// GETTERS
	
	public char GetSymbol() {return _symbol;}
	public char GetColor() {return _color;}
	public Player GetPlayer() {return _player;}
	
	// SETTERS
	
	/**
	 * Sets card owner.
	 * @param player
	 * Player's name.
	 * @return
	 * this.
	 */
	public Card SetPlayer(Player player) 
	{
		_player = player;
		return this;
	}
	
	// METHODS
	
	/**
	 * Get card description. Result is like [7 ♥].
	 * @return
	 * Description of a card.
	 */
	public String GetDesc()
	{
		final StringBuilder retr = new StringBuilder();
		retr.append('[');
		retr.append(_symbol);
		retr.append(' ');
		retr.append(_color);
		retr.append(']');
		return retr.toString();
	}

	/**
	 * Resets ownership.<br />
	 * NOTE: this functionality is tested from Deck class tester.
	 * @return
	 * this
	 */
	public Card Reset() {
		_player = null;		
		return this;
	}
}
