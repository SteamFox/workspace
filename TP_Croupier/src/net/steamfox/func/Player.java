package net.steamfox.func;

public class Player 
{
	//FIELDS
	
	private String _name;
	
	// C's & D's
	
	public Player(String name)
	{
		_name = name;
	}
	
	// GETTERS
	
	public String GetName() {return _name;}
}
