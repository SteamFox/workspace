package net.steamfox.func;

public class CardHelper 
{	
	// STATIC
	
	/**
	 * Get English name of card's symbol.
	 * @param symbol
	 * Symbol to convert.
	 * @return
	 * Card name, either Spade, Heart, Diamond, Club or null
	 */
	public static String GetName(char symbol)
	{
		switch(symbol)
		{
		case '\u2660':
			return "Spade";
		case '\u2666':
			return "Diamond";
		case '\u2663':
			return "Club";
		case '\u2764':
			return "Heart";
		default:
			return null;
		}
	}
	
	/**
	 * Convert English card symbol name to Unicode character
	 * which represents that symbol.
	 * @param name
	 * English name of symbol.
	 * @return
	 * Unicode character representing given name or (char)0.
	 */
	public static char GetSymbol(String name)
	{
		switch(name)
		{
		case "Spade":
			return '\u2660';
		case "Diamond":
			return '\u2666';
		case "Club":
			return '\u2663';
		case "Heart":
			return '\u2764';
		default:
			return 0;
		}
	}
}
