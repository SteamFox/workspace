package net.steamfox.func;

public class Table 
{
	//FIELDS
	
	private Deck _deck;
	private Player[] _players;
	
	// C's & D's
	
	public Table(String[] playerNames)
	{
		_deck = new Deck();
		_players = new Player[playerNames.length];
		
		for(int i = 0; i < playerNames.length; i++)
		{
			_players[i] = new Player(playerNames[i]);
		}
	}
	
	// GETTERS
	
	public Deck GetDeck() {return _deck;}
	public Player[] GetPlayers() {return _players;}
	
	// METHODS

	/**
	 * Shuffles the deck and hands n cards to each player
	 * @param n
	 * How many cards should be given to each player.
	 * @return
	 * null if failed, otherwise this.
	 */
	public Table StartNewGame(int n)
	{
		_deck.ResetOwnership();
		_deck.Shuffle();
		if(_deck.HandCards(_players, n) == null)
		{
			return null;
		} 
		return this;
	}
	
	public static void main(String[] args)
	{
		Table mainTable = new Table(
				new String[] 
						{"Joshua", "Heisenberg", "Iggy", "Mark"});
		
		if(mainTable.StartNewGame(2) != null)
			mainTable.GetDeck().PrintDeck();
	}
}
