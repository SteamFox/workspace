package net.steamfox.tester;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.steamfox.func.Card;
import net.steamfox.func.CardHelper;
import net.steamfox.func.Player;

public class CardTester
{
	Card _testingObject1;
	
	@Before
	public void SetUp()
	{
		_testingObject1 = new Card(CardHelper.GetSymbol("Spade"), '7');
	}
	
	@After
	public void TearDown()
	{
		
	}
	
	@Test
	public void Test_IsDescFormatOK()
	{
		StringBuilder dest = new StringBuilder();
		dest.append('[');
		dest.append(CardHelper.GetSymbol("Spade"));
		dest.append(" 7]");
		assertEquals(dest.toString(), _testingObject1.GetDesc());
	}
	
	@Test
	public void Test_CanGetSetPlayer()
	{
		assertTrue(_testingObject1.GetPlayer() == null);
		Player marvin = new Player("Marvin");
		assertSame(
				_testingObject1.SetPlayer(marvin)
				.GetPlayer().GetName(),
				"Marvin");
	}
}
