package net.steamfox.tester;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.steamfox.func.Card;
import net.steamfox.func.Deck;
import net.steamfox.func.Player;

public class DeckTester 
{
	private Deck _testingObejct1;
	
	@Before
	public void SetUp()
	{
		_testingObejct1 = new Deck();
	}
	
	@After
	public void TerrDown()
	{
		
	}
	
	/**
	 * NOTE: This test is unreliable, since there is chance that shuffle
	 * will return same deck.
	 */
	@Ignore
	@Test
	public void Test_DoesShuffle()
	{
		Deck temp = new Deck();
		temp.Shuffle();
		
		int count = 0;
		for(int i = 0; i < _testingObejct1.GetCards().size(); i++)
		{
			if(temp.GetCards().get(i) == _testingObejct1.GetCards().get(i)) {
				count++;
			}
		}
		assertSame(count, 52);
	}

	@Test
	public void Test_DoesPassCardsToPlayer()
	{
		Player tempPlayer = new Player("Miriam");
		_testingObejct1.PassMultipleCardsToPlayer(tempPlayer, 5);
		
		int count = 0;
		for(Card c : _testingObejct1.GetCards())
		{
			if(c.GetPlayer() != null && 
					c.GetPlayer().GetName() == "Miriam")
			{
				count++;
			}
		}
		assertSame(5, count);
	}
	
	@Test
	public void Test_DoesResetOwnership()
	{
		_testingObejct1.ResetOwnership();
		for(Card card : _testingObejct1.GetCards())
		{
			assertNull(card.GetPlayer());
		}
	}
}
